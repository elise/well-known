FROM rust:latest

EXPOSE 80

WORKDIR /usr/src/well-known
COPY . .

RUN cargo install --path .

CMD ["well-known", "--config=/config.yaml"]