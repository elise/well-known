use std::{borrow::Cow, net::SocketAddr};

use config::{Config, File};
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "mode", content = "content")]
pub enum WellKnownRoute {
	Redirect(url::Url),
	Json(String),
}

#[derive(Clone, Debug, Deserialize)]
pub struct WellKnownConfigEntry {
	pub path: String,
	#[serde(flatten)]
	pub route: WellKnownRoute,
}

#[derive(Clone, Debug, Deserialize)]
pub struct WellKnownConfig {
	pub bind: SocketAddr,
	pub log_level: log::LevelFilter,
	pub entries: Vec<WellKnownConfigEntry>,
}

impl WellKnownConfig {
	pub fn read() -> Result<Self, config::ConfigError> {
		let mut config = Config::new();
		let path = std::env::args()
			.find_map(|val| val.strip_prefix("--config=").map(String::from))
			.map(Cow::Owned)
			.unwrap_or(Cow::Borrowed("config.yaml"));
		config.merge(File::with_name(&path))?;
		config.try_into()
	}
}
