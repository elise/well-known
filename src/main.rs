mod settings;
use std::sync::Arc;

use axum::{extract, http::Method, response::Response, routing::get, AddExtensionLayer, Router};
use settings::WellKnownConfig;
use tower_http::cors::{Any, CorsLayer};

async fn handle_well_known(
	config: extract::Extension<Arc<WellKnownConfig>>,
	url: extract::OriginalUri,
) -> impl axum::response::IntoResponse {
	log::info!("Serving {}", url.0);

	config
		.entries
		.iter()
		.find_map(|entry| {
			match url.0.path().trim_start_matches("/.well-known") == entry.path.as_str() {
				true => Some(match &entry.route {
					settings::WellKnownRoute::Redirect(r_url) => {
						let mut r_url = r_url.clone();

						if let Some(req_query) = url.0.query() {
							r_url.set_query(Some(req_query));
						}

						Response::builder()
							.status(axum::http::StatusCode::FOUND)
							.header(axum::http::header::LOCATION, r_url.as_str())
							.body(axum::body::boxed(axum::body::Full::from(
								axum::body::Bytes::new(),
							)))
							.unwrap()
					}
					settings::WellKnownRoute::Json(json) => Response::builder()
						.status(axum::http::StatusCode::OK)
						.header(
							axum::http::header::CONTENT_TYPE,
							axum::http::HeaderValue::from_static("application/json; charset=utf-8"),
						)
						.body(axum::body::boxed(axum::body::Full::from(
							serde_json::to_vec(
								&serde_json::from_str::<serde_json::Value>(json.as_str()).unwrap(),
							)
							.unwrap(),
						)))
						.unwrap(),
				}),
				false => None,
			}
		})
		.unwrap_or_else(|| panic!("Unable to find entry for {}", url.0))
}

#[tokio::main]
async fn main() {
	log::info!("Starting well-known");
	let config = settings::WellKnownConfig::read().expect("Failed to read config!");

	fern::Dispatch::new()
		.format(|out, message, record| {
			out.finish(format_args!(
				"{}[{}][{}] {}",
				chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
				record.target(),
				record.level(),
				message
			))
		})
		.level(config.log_level)
		.chain(std::io::stdout())
		.apply()
		.unwrap();

	let mut router = Router::new();

	for entry in &config.entries {
		assert!(entry.path.starts_with('/'), "Path does not start with '/'");
		let path = format!("/.well-known{}", entry.path);

		if let settings::WellKnownRoute::Json(json) = &entry.route {
			serde_json::from_str::<serde_json::Value>(json.as_str())
				.unwrap_or_else(|_| panic!("Failed to parse JSON body for {}", entry.path));
		}

		log::info!("Adding route {}", path);

		router = router.route(path.as_str(), get(handle_well_known));
	}

	router = router.layer(AddExtensionLayer::new(Arc::new(config.clone()))).layer(
		CorsLayer::new()
			.allow_methods(vec![Method::GET, Method::POST, Method::DELETE, Method::OPTIONS])
			.allow_origin(Any)
			.allow_headers(Any),
	);

	log::info!("Binding to {}", config.bind);

	axum::Server::bind(&config.bind).serve(router.into_make_service()).await.unwrap();
}
